<?php
/**
 * Elogic Breeze theme
 */

use Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(ComponentRegistrar::THEME, 'frontend/Elogic/breeze', __DIR__);
